$(window).load(function(){
	var songs = $('.audio');
	songs.each(function(){
		addPlayButton(this);
	})

	$("#audios_list").bind("DOMNodeInserted", function(a) {
    	el = a.target;
    	if ($(el).is('.audio'))
    		addPlayButton(el);
	});

})

function addPlayButton(el){
	var play_button = $('<span class="simul_play">');
	play_button.click(addToQueue);
	$(el).prepend(play_button);
}

function addToQueue(){
	var link = $(this).parent().find('input').val();
	// console.log(link);
	var title = $(this).siblings('.area').find('.title_wrap').text();

	$.ajax({
		url: 'http://yura/new_song.php',
		type: 'POST',
		dataType: 'json',
		data: {
			link: link,
			title: title
		},
		success: function(response){
			console.log(response)
		},
		complete: function(){

		},
		error: function(response){
			alert('error');
			console.log(response);
		}
	})
}