var audio,
	clock,
	client_list,
	start_time = false,
	timeoutHandler = false;

$(window).load(function(){
	audio = $('#audio');
	a = audio[0]
	clock = $('#clock');
	client_list = $('#client_list');
	song_name = $('.song_name');

	audio.on('waiting', function(){
		audio.trigger('pause');
		debug('Stopped because of fucking buffer.')
	})

	// syncTime();
	// syncTime2();
	syncTime3();

})


function syncTime(){

	var xhr = new XMLHttpRequest();
	var start = new Date().getTime();
    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4){
        	var now = new Date().getTime();
        	var requestDuration = now - start;
        	var serverTime = Math.round(parseInt(xhr.responseText) + requestDuration/2);
        	serverTimeDiff = serverTime - now;

		        // console.log('now: ' + now);
		        debug('requestDuration: ' + requestDuration);
		        // console.log('response: ' + xhr.responseText);
		        // console.log('serverTime: ' + serverTime);
		        debug('serverTimeDiff: ' + serverTimeDiff);

		        // console.log('getServerTime: ' + getServerTime());

		    serverClock();
		    getActions();
		    setInterval(getActions, 2000);
        }

    };

    xhr.open("POST", "time.php", false);
    xhr.send();
}

function syncTime2(){
	var clientTimestamp = (new Date()).valueOf();

	$.getJSON('time2.php?ct='+clientTimestamp, function( data ) {
	    var nowTimeStamp = (new Date()).valueOf();
	    var serverClientRequestDiffTime = data.diff;
	    var serverTimestamp = data.serverTimestamp;
	    var serverClientResponseDiffTime = nowTimeStamp - serverTimestamp;
	    var responseTime = (serverClientRequestDiffTime - nowTimeStamp + clientTimestamp - serverClientResponseDiffTime )/2

	    // var syncedServerTime = new Date((new Date()).valueOf() + (serverClientResponseDiffTime - responseTime));
	    serverTimeDiff = serverClientResponseDiffTime - responseTime;

    	console.log('serverTimeDiff2: ' + serverTimeDiff);
    	debug('serverTimeDiff2: ' + serverTimeDiff)

	    serverClock();
	    getActions();
	    setInterval(getActions, 2000);
	});
}

function syncTime3(){
	var clientTimestamp = (new Date()).valueOf();

	var xhr = new XMLHttpRequest();
	var start = new Date().getTime();
    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4){
        	var now = new Date().getTime();
        	var requestDuration = now - start;
        	serverTimeDiff = Math.round(parseInt(xhr.responseText) + requestDuration/2);
        	// serverTimeDiff = serverTime - now;

		        // console.log('now: ' + now);
		        debug('requestDuration: ' + requestDuration);
		        // console.log('response: ' + xhr.responseText);
		        // console.log('serverTime: ' + serverTime);
		        debug('serverTimeDiff: ' + serverTimeDiff);

		        // console.log('getServerTime: ' + getServerTime());

		    serverClock();
		    getActions();
		    setInterval(getActions, 2000);
        }
    };

    xhr.open("GET", "time3.php?ct=" + clientTimestamp, false);
    xhr.send();


	/*$.getJSON('time3.php?ct='+clientTimestamp, function( data ) {
	    var nowTimeStamp = (new Date()).valueOf();
	    // var serverClientRequestDiffTime = data.diff;
	    var serverTimestamp = data.serverTimestamp;
	    var serverClientRequestDiffTime = serverTimestamp - clientTimestamp;
	    var serverClientResponseDiffTime = nowTimeStamp - serverTimestamp;
	    var req_resp = nowTimeStamp - clientTimestamp ;
	    // var responseTime = (serverClientRequestDiffTime - nowTimeStamp + clientTimestamp - serverClientResponseDiffTime )/2



		debug('clientTimestamp: ' + clientTimestamp);
		debug('nowTimeStamp: ' + nowTimeStamp);
		debug('serverClientRequestDiffTime: ' + serverClientRequestDiffTime);
		debug('data.diff: ' + data.diff);
		debug('serverTimestamp: ' + serverTimestamp);
		debug('serverClientResponseDiffTime: ' + serverClientResponseDiffTime);
		debug('req_resp: ' + req_resp);

	    serverTimeDiff = clientTimestamp - serverClientRequestDiffTime- serverTimestamp

	    serverTimeDiff = (req_resp - serverClientRequestDiffTime - serverClientResponseDiffTime)/2

	    // var syncedServerTime = new Date((new Date()).valueOf() + (serverClientResponseDiffTime - responseTime));
	    // serverTimeDiff = serverClientResponseDiffTime - responseTime;
    	debug('serverTimeDiff3: ' + serverTimeDiff)

	    serverClock();
	    getActions();
	    setInterval(getActions, 2000);
	});*/
}

function getServerTime(){
	return new Date().getTime() + serverTimeDiff;
}

function play(){
	if (audio.prop('readyState') === 4){
		debug('networkState: ' + audio.prop('networkState'))
		audio.trigger('play');
	}
}

function getActions(){
	$.getJSON('actions.json.php', function(response){

		var left = response.start_at - getServerTime();
		if (left > 0 && start_time != response.start_at){
			console.log(left + ' left');
			start_time = response.start_at;
			audio.attr('src', response.song);
			audio.load();

			song_name.text(response.title)

			if (timeoutHandler){
				clearTimeout(timeoutHandler);
			}
			timeoutHandler = setTimeout(play, response.start_at - getServerTime());
		}

		client_list.html('');
		response.clients.forEach(function(client){
			var li = $('<li><b>' + client.ip_address + '</b> (' + client.browser + ')</li>');
			client_list.append(li);
		})
	})
}

function serverClock(){
	var serverTime = new Date(getServerTime());
	var h = serverTime.getHours();
	var m = serverTime.getMinutes();
	var s = serverTime.getSeconds();
	if (s < 10)
		s = '0' + s;
	var ms = Math.round(serverTime.getMilliseconds()/100);
	ms = ms == 10 ? 0 : ms;
	// if (ms == 10)
		// ms = '00' + ms;

	clock.text(h + ':' + m + ':' + s + '.' + ms);
	setTimeout(serverClock, 50)
	// serverClock();
}

function debug(text){
	$('#debug').append(text + '<br>')
}