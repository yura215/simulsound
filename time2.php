<?php

header('Content-Type: application/json; charset=utf-8');
$clientTime = (float) $_GET["ct"] * 1;
$serverTimestamp = round(microtime(true)*1000);
$serverClientRequestDiffTime = $serverTimestamp - $clientTime;
echo "{\"diff\":$serverClientRequestDiffTime,\"serverTimestamp\":$serverTimestamp}";