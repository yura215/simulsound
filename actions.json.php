<?php

require_once('config.php');

function detectBrowser(){

	$browsers = array(
		'MSIE'			=> 'Internet explorer',
		'Trident'		=> 'Internet explorer',
		'Firefox'		=> 'Mozilla Firefox',
		'Chrome'		=> 'Google Chrome',
		'Opera Mini'	=> 'Opera Mini',
		'Opera'			=> 'Opera',
		'Safari'		=> 'Safari',
		);

	foreach ($browsers as $key => $browserName) {
		if (strpos($_SERVER['HTTP_USER_AGENT'], $key)){
			return $browserName;
		}
	}

	return 'Unknown';
}

$format = "REPLACE INTO clients (user_agent, ip_address, browser) VALUES ('%s', '%s', '%s')";
$query = sprintf($format, $_SERVER['HTTP_USER_AGENT'], $_SERVER['REMOTE_ADDR'], detectBrowser());
$res = $mysqli->query($query) or print_r($mysqli->error . "\n");

$query = "SELECT ip_address, browser, user_agent = '$_SERVER[HTTP_USER_AGENT]' as me FROM clients WHERE NOW() - updated_at < 5";
$res = $mysqli->query($query) or print_r($mysqli->error . "\n");
$clients = array();
while ($row = $res->fetch_assoc()) {
	$clients[] = $row;
}

$query = "SELECT link, title, start_at FROM songs ORDER BY start_at DESC LIMIT 1";
$res = $mysqli->query($query);
// var_dump($res);
// exit();
$row = $res->fetch_assoc();


echo json_encode(array(
	'song'		=> $row['link'],
	'title'		=> $row['title'],
	'clients' 	=> $clients,
	'start_at' 	=> $row['start_at']
	));
