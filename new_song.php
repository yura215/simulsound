<?php

require_once('config.php');

$success = false;

if (isset($_POST['link']) and !empty($_POST['link'])){
	$start_at = round((microtime(true) + 10) * 1000);
	$query = sprintf("INSERT INTO songs SET link = '%s', title = '%s', start_at = %s", $_POST['link'], $_POST['title'], $start_at);
	$success = $mysqli->query($query);
}

echo json_encode(array('success' => $success, 'message' => $mysqli->error));
